make:
	cat Makefile

update-internal-py-os-config:
	cd bard-internal && make update-py-os-config

update-external-py-os-config:
	cd bard-external && make update-py-os-config