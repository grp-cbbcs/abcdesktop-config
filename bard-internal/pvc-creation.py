"""
    Batch create pods with persistent volumes
    by hydrating templates with values from a CSV file.
"""
from contextlib import contextmanager
import csv
import os
import subprocess
import sys
import tempfile

POD_TEMPL = '''
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-nfs-abc-{pvc_name}
  namespace: abcdesktop-ns
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 200Ti

---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv-nfs-abc-{pvc_name}
  namespace: abcdesktop-ns
spec:
  capacity:
    storage: 200Ti
  volumeMode: Filesystem
  accessModes:
    - ReadWriteMany
  persistentVolumeReclaimPolicy: Retain
  mountOptions:
    - nfsvers=3
  nfs:
    path: {pvc_path}
    server: {pvc_server}
  claimRef:
    namespace: abcdesktop-ns
    name: pvc-nfs-abc-{pvc_name}
'''


def kubectl_apply(path):
    """Apply a manifest."""
    process = subprocess.run(
        ['kubectl', 'apply', '-f', path],
        stdout=subprocess.PIPE, universal_newlines=True,
    )
    return process.returncode, process.stdout


@contextmanager
def TemplateFile(templ, **kw):
    """Render a template and return it's path."""
    with tempfile.NamedTemporaryFile(mode='w', suffix='.yml', delete=False) as f:
        f.write(templ.format(**kw))
    yield f.name
    os.remove(f.name)


def run_main():
    """Run the app."""
    assert len(sys.argv) > 1, 'CSV file name is required.'
    with open(sys.argv[1], mode='r', encoding='utf8') as f:
        for spath, pvc, serv in csv.reader(f, delimiter=','):
            context = {
                'pvc_path': spath,
                'pvc_name': pvc,
                'pvc_server': serv,
            }
            with TemplateFile(POD_TEMPL, **context) as path:
                kubectl_apply(path)


if __name__ == '__main__':
    run_main()
