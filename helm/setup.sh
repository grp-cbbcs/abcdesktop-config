export NAMESPACE=abcdesktop-test-ns

# openssl genrsa -out abcdesktop_jwt_desktop_payload_private_key.pem 1024
# openssl rsa -in abcdesktop_jwt_desktop_payload_private_key.pem -outform PEM -pubout -out _abcdesktop_jwt_desktop_payload_public_key.pem
# openssl rsa -pubin -in _abcdesktop_jwt_desktop_payload_public_key.pem -RSAPublicKey_out -out abcdesktop_jwt_desktop_payload_public_key.pem
# openssl genrsa -out abcdesktop_jwt_desktop_signing_private_key.pem 1024
# openssl rsa -in abcdesktop_jwt_desktop_signing_private_key.pem -outform PEM -pubout -out abcdesktop_jwt_desktop_signing_public_key.pem
# openssl genrsa -out abcdesktop_jwt_user_signing_private_key.pem 1024
# openssl rsa -in abcdesktop_jwt_user_signing_private_key.pem -outform PEM -pubout -out abcdesktop_jwt_user_signing_public_key.pem

# kubectl create secret generic abcdesktopjwtdesktoppayload --from-file=abcdesktop_jwt_desktop_payload_private_key.pem --from-file=abcdesktop_jwt_desktop_payload_public_key.pem --namespace=$NAMESPACE
# kubectl create secret generic abcdesktopjwtdesktopsigning --from-file=abcdesktop_jwt_desktop_signing_private_key.pem --from-file=abcdesktop_jwt_desktop_signing_public_key.pem --namespace=$NAMESPACE
# kubectl create secret generic abcdesktopjwtusersigning --from-file=abcdesktop_jwt_user_signing_private_key.pem --from-file=abcdesktop_jwt_user_signing_public_key.pem --namespace=$NAMESPACE

# curl https://raw.githubusercontent.com/abcdesktopio/conf/main/reference/od.config.3.5 --output od.config
# # kubectl create configmap abcdesktop-config --from-file=od.config -n $NAMESPACE

# curl https://raw.githubusercontent.com/abcdesktopio/conf/main/reference/abcdesktop-3.5.yaml --output abcdesktop-3.5.yaml
# # kubectl create -f https://raw.githubusercontent.com/abcdesktopio/conf/main/kubernetes/abcdesktop-3.5.yaml
VERSION="3.5"

ABCDESKTOP_YAML_SOURCE="https://raw.githubusercontent.com/abcdesktopio/conf/main/kubernetes/abcdesktop-$VERSION.yaml"
OD_CONFIG_SOURCE="https://raw.githubusercontent.com/abcdesktopio/conf/main/reference/od.config.$VERSION"

# $1 message
display_message_result() {
    exit_code="$?"
    if [ "$exit_code" -eq 0 ]; then
        display_message "$1" "OK"
    else
        display_message "$1 error $exit_code" "KO"
        # by default force=0, exit
        if [ "$FORCE" -eq 0 ]; then
            exit 1
        fi
    fi
}

# $1 message
display_section() {
    printf "\033[0;1;4m%s\033[0;0m\n" "$1"
}

# $1 message
# $2 status
display_message() {
    # ${2^^}: bad substitution, use "${2}"
    # use printf instead of echo for better compatibility sh zsh bash
    case "${2}" in
    "OK") COLOR="\033[0;32m" ;;
    "KO") COLOR="\033[0;31m" ;;
    "ERROR") COLOR="\033[0;31m" ;;
    "WARN") COLOR="\033[0;33m" ;;
    "INFO") COLOR="\033[1;34m" ;;
    esac
    printf "[$COLOR%s\033[0;0m] %s\n" "$2" "$1"
}

# create abcdesktop.yaml file
if [ -f abcdesktop.yaml ]; then
    display_message "use local file abcdesktop.yaml" "OK"
    ABCDESKTOP_YAML=abcdesktop.yaml
else
    curl --progress-bar "$ABCDESKTOP_YAML_SOURCE" --output abcdesktop.yaml
    display_message_result "downloaded source $ABCDESKTOP_YAML_SOURCE"
    if [ ! -z "$IMAGEPULLPOLICY" ]; then
        sed -i "s/IfNotPresent/$IMAGEPULLPOLICY/g" abcdesktop.yaml
        display_message_result "update imagePullPolcy to $IMAGEPULLPOLICY"
    fi
fi

# create od.config file
if [ -f od.config ]; then
    display_message "use local file od.config" "OK"
else
    curl --progress-bar "$OD_CONFIG_SOURCE" --output od.config
    display_message_result "downloaded source $OD_CONFIG_SOURCE"
    if [ ! -z "$IMAGEPULLPOLICY" ]; then
        sed -i "s/IfNotPresent/$IMAGEPULLPOLICY/g" od.config
        display_message_result "update imagePullPolcy to $IMAGEPULLPOLICY"
    fi
fi
